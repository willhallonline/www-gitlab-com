features:
  secondary:
    - name: "Improved Geo upgrade documentation"
      available_in: [premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/administration/geo/replication/updating_the_geo_nodes.html"
      reporter: fzimmer
      stage: enablement
      gitlab_com: false
      issue_url: "https://gitlab.com/groups/gitlab-org/-/epics/1935"
      description: |
        As part of our effort to [simplify the Geo upgrade process](https://gitlab.com/groups/gitlab-org/-/epics/1450),
        we reworked large parts of the Geo upgrade documentation.
        GitLab Geo can be deployed in different configurations and depending
        on the configuration Geo upgrades require different steps. At this
        moment, upgrading Geo is still highly manual and can involve many
        individual steps. To improve this in an iterative way, we focused first
        on [improving the Geo upgrade documentation](https://gitlab.com/groups/gitlab-org/-/epics/1935)
        itself. This ensures that the documentation is up-to-date and covers all
        use cases.

        We rewrote the [general update steps](https://gitlab.com/gitlab-org/gitlab/issues/12773),
        [archived old update steps](https://gitlab.com/gitlab-org/gitlab/issues/11881),
        [updated zero-downtime upgrade instructions for simple deployments](https://gitlab.com/gitlab-org/gitlab/issues/14000)
        and investigated [many other parts of the documentation](https://gitlab.com/groups/gitlab-org/-/epics/1935).

        We are also working on instructions for zero-downtime updates of
        [a multi-node, high-availability Geo cluster](https://gitlab.com/gitlab-org/gitlab/issues/7602);
        however, these instructions require more testing before we release them.

        Following this work, we will work on better automation, improved
        testing, and making certain upgrade procedures more robust.